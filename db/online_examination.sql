-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 30, 2020 at 11:42 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_examination`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_table`
--

DROP TABLE IF EXISTS `admin_table`;
CREATE TABLE IF NOT EXISTS `admin_table` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_email_address` varchar(150) COLLATE utf16_unicode_ci NOT NULL,
  `admin_password` varchar(150) COLLATE utf16_unicode_ci NOT NULL,
  `admin_verfication_code` varchar(100) COLLATE utf16_unicode_ci NOT NULL,
  `admin_type` enum('浡獴敲','獵扟浡獴敲') COLLATE utf16_unicode_ci NOT NULL,
  `admin_created_on` datetime NOT NULL,
  `email_verified` enum('湯','祥?') COLLATE utf16_unicode_ci NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `course_table`
--

DROP TABLE IF EXISTS `course_table`;
CREATE TABLE IF NOT EXISTS `course_table` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_title` varchar(255) COLLATE utf16_unicode_ci NOT NULL,
  `course_data` text COLLATE utf16_unicode_ci NOT NULL,
  `course_image` longblob NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `course_table`
--

INSERT INTO `course_table` (`course_id`, `course_title`, `course_data`, `course_image`) VALUES
(1, 'THE PRESENT SIMPLE', 'The present simple in english is used to make reference to events which occur iteratively (n times) \r\nTemporal adverbs : Always each/ every day \r\nOften Sometimes, from time to time, every now and then, once in a while Rarely Never/ ever \r\nEx : I get up every day \r\nThe present simple in english is used to make reference to permanent states \r\nEx : Water freezes at 0° centigrade \r\nSubjective verbs are considered as permanent states in English therefore they tend to be used in the present simple. \r\nEx : I love NY \r\n', ''),
(2, 'THE PRESENT CONTINUOUS', 'The present continuous is used to refer to processes or states which are in developement at time of speaking. \r\nTemporal adverbs : now, currently, presently, at this time. \r\nEx : I am writting/reading a grammar course. \r\nPARADOX!!!  \r\nHe is always asking me stupid questions. ( he\'s getting on my nerves !) \r\n', '');

-- --------------------------------------------------------

--
-- Table structure for table `online_exam_table`
--

DROP TABLE IF EXISTS `online_exam_table`;
CREATE TABLE IF NOT EXISTS `online_exam_table` (
  `online_exam_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `online_exam_title` varchar(250) CHARACTER SET latin1 NOT NULL,
  `online_exam_datetime` datetime NOT NULL,
  `online_exam_duration` varchar(30) CHARACTER SET latin1 NOT NULL,
  `total_question` int(5) NOT NULL,
  `marks_per_right_answer` varchar(30) CHARACTER SET latin1 NOT NULL,
  `marks_per_wrong_answer` varchar(30) CHARACTER SET latin1 NOT NULL,
  `online_exam_created_on` datetime NOT NULL,
  `online_exam_status` enum('Pending','Created','Started','Completed') CHARACTER SET latin1 NOT NULL,
  `online_exam_code` varchar(100) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`online_exam_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf16 COLLATE=utf16_unicode_ci;

--
-- Dumping data for table `online_exam_table`
--

INSERT INTO `online_exam_table` (`online_exam_id`, `admin_id`, `online_exam_title`, `online_exam_datetime`, `online_exam_duration`, `total_question`, `marks_per_right_answer`, `marks_per_wrong_answer`, `online_exam_created_on`, `online_exam_status`, `online_exam_code`) VALUES
(1, 1, 'BE + ING', '2020-04-19 01:50:00', '5', 10, '5', '1', '2020-04-18 20:21:34', 'Pending', '11c5b163d8510fa04ec23280a13af77c'),
(2, 1, 'TOS', '2020-04-22 01:03:00', '20', 10, '1', '1', '2020-04-23 00:00:00', 'Created', '');

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

DROP TABLE IF EXISTS `option_table`;
CREATE TABLE IF NOT EXISTS `option_table` (
  `option_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `option_number` int(2) NOT NULL,
  `option_title` varchar(250) NOT NULL,
  PRIMARY KEY (`option_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `option_table`
--

INSERT INTO `option_table` (`option_id`, `question_id`, `option_number`, `option_title`) VALUES
(1, 1, 1, ''),
(2, 1, 2, ''),
(3, 1, 3, ''),
(4, 1, 4, ''),
(5, 4, 1, ''),
(6, 4, 2, ''),
(7, 4, 3, ''),
(8, 4, 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `question_table`
--

DROP TABLE IF EXISTS `question_table`;
CREATE TABLE IF NOT EXISTS `question_table` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `online_exam_id` int(11) NOT NULL,
  `question_title` text NOT NULL,
  `answer_title_a` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `answer_title_b` varchar(255) NOT NULL,
  `answer_title_c` varchar(255) NOT NULL,
  `answer_title_d` varchar(255) NOT NULL,
  `right_answer` char(1) NOT NULL,
  `question_duration` int(2) NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_table`
--

INSERT INTO `question_table` (`question_id`, `online_exam_id`, `question_title`, `answer_title_a`, `answer_title_b`, `answer_title_c`, `answer_title_d`, `right_answer`, `question_duration`) VALUES
(1, 1, 'The company _____  spouses of employees in the invitation to the banquet.', 'are included', 'have included', 'is including', 'has including\r\n', 'c', 10),
(2, 1, 'The head of operations  _____  to the management convention.', 'going', 'are going', 'go', 'is going', 'd', 12),
(3, 1, 'The technicians in the research division _____ the process confidential.', 'is keeping', 'are keeping', 'to keep', 'has kept', 'b', 10),
(4, 1, 'The Burlington Foreign Fund is not _____ new accounts at present.', 'accepted', 'accepts', 'accepting', 'acceptance', 'c', 10),
(5, 1, 'Those  _____  the CCB Studios as members of a viewing  audience should proceed to Studio B', 'who visiting', 'which visit', 'whose visit', 'who are visiting', 'd', 10),
(6, 2, '______ the annual report available yet?', 'are', 'is', 'being', 'have', 'b', 10),
(7, 2, 'The office ______ that all employees park in their assigned spaces.', 'are required', 'ask', 'requires', 'has require', 'c', 10),
(8, 2, 'Employees ______ tasks that are repetitive', 'are disliking', 'disliking', 'disliked', 'dislike', 'd', 10),
(9, 2, 'At the end of the year, the company ______ up a picnic for the employees.', 'puts', 'put', 'putting', 'are putting', 'a', 10),
(10, 2, 'The last train to Hamburg _____  at 10:30.', 'depart', 'departs', 'to depart', 'departing', 'b', 10),
(11, 1, 'Market researchers _____  international markets as primary targets for the upcoming fiscal year.', 'were looked', 'are looked', 'are looking', 'is look', 'c', 10),
(12, 2, 'The restaurant ________ open on weekends, _not on holidays.', 'bing', 'are', 'be', 'is', 'd', 10);

-- --------------------------------------------------------

--
-- Table structure for table `user_exam_enroll_table`
--

DROP TABLE IF EXISTS `user_exam_enroll_table`;
CREATE TABLE IF NOT EXISTS `user_exam_enroll_table` (
  `user_exam_enroll_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `attendance_status` enum('Absent','Present') NOT NULL,
  PRIMARY KEY (`user_exam_enroll_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_exam_question_answer`
--

DROP TABLE IF EXISTS `user_exam_question_answer`;
CREATE TABLE IF NOT EXISTS `user_exam_question_answer` (
  `user_exam_question_answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `user_answer_option` enum('0','1','2','3','4') NOT NULL,
  `marks` varchar(20) NOT NULL,
  PRIMARY KEY (`user_exam_question_answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
CREATE TABLE IF NOT EXISTS `user_table` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_email_address` varchar(250) NOT NULL,
  `user_password` varchar(150) NOT NULL,
  `user_verfication_code` varchar(100) NOT NULL,
  `user_name` varchar(150) NOT NULL,
  `user_gender` enum('Male','Female') NOT NULL,
  `user_address` text NOT NULL,
  `user_mobile_no` varchar(30) NOT NULL,
  `user_image` varchar(150) NOT NULL,
  `user_created_on` datetime NOT NULL,
  `user_email_verified` enum('no','yes') NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_table`
--

INSERT INTO `user_table` (`user_id`, `user_email_address`, `user_password`, `user_verfication_code`, `user_name`, `user_gender`, `user_address`, `user_mobile_no`, `user_image`, `user_created_on`, `user_email_verified`) VALUES
(1, 'mehdiyahyavi@gmail.com', '$2y$10$EjV2vsnN3.3Ri0KsMk19P.h7O.UBHELgUHULRd2o24FpXeN5QO4ky', 'd1c89f614de1216edd03baee80822a87', 'Mehdi', 'Male', 'Iran Mazandaran', '009111542265', '5e9b5f14dbad7.png', '2020-04-18 20:12:04', 'yes');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
