<?php
include "header.php";
$courseid = $_GET["courseid"];
$questionid = '';
?>

<div class="container" >

    <div class="col-md-12 quiz-content" id="question" disabled="true">
        <div class="title row">
            <div align="center" class="col"><h2>Quiz</h2><b> YOU HAVE 15 SECONDS PER QUESTION LETS START WITH 10 QUESTIONS !!!</b></div>
            
            <div align="left" id="duration"></div>
        </div>
        <div class="container"  id="completeexam" style="display: none;margin: 10px;">
            <h2 align="center">Your DONE!</h2>
            <button class="btn btn-secondary" style="width: 100%; margin-bottom: 5px">RETURN</button>
            <br>
            <button class="btn btn-primary btn-danger" style="width: 100%">CONTINUE</button>
        </div>
        <div class="container" id="questionform">
            <div class='question'><h3 id="qtitle"> </h3></div>
            <div class='row' id="" style="display: block">
                <div class='col-md-6 answer'>
                    <li>
                        <ul>
                            <input class='custom-radio' type='radio' name='answer' value='a' id='a1'>
                            <label id="answ1" for='a1'> </label>
                        </ul>
                        <ul>
                            <input class='custom-radio' type='radio' name='answer' value='b' id='a2'>
                            <label id="answ2" for='a2'></label>
                        </ul>
                        <ul>
                            <input class='custom-radio' type='radio' name='answer' value='c' id='a3'>
                            <label id="answ3" for='a3'></label>
                        </ul>
                        <ul>
                            <input class='custom-radio' type='radio' name='answer' value='d' id='a4'>
                            <label id="answ4" for='a4'></label>
                        </ul>
                    </li>
                    <hr>
                       <div align="center"> <h4 id="answerStatus"></h4></div>
                    <button class='btn btn-primary btnsubmit' disabled="true" style="width: 100%"  type='submit'>
                        Submit
                    </button>
                </div>
                <div class='col-md-4 timer' id='CountDown'></div>
            </div>
        </div>
    </div>

    <script>
        var answer; //STORE RIGHT ANSWER FROM DB
        var exam_id = <?php echo $courseid ?>; //GET EXAM_ID THAT EQUAL TO COURSE_ID
        var questionid; //STORE NEXT QUESTION_ID
        var qcounter =0; //QUESTION COUNTER
        var totalQuestion;
        var duration;
        var question_id;
        //LOAD QUESTIONS
        $(document).ready(function () {
            Load_Total_Time(exam_id);
            load_data();

            function Load_Total_Time(exam = ''){
                //console.log(exam);
                $.ajax({
                    url:"user_ajax.php",
                    method: "POST",
                    data:{examid: exam},
                    success: function (recivedata) {
                        var da = JSON.parse(recivedata);
                        totalQuestion = da.totalQuestionItems;
                        duration = da.totalduration;
                        $('#duration').attr('data-timer',duration).TimeCircles().start();
                    }

                })
            }
            function load_data(qid = '') {
                $.ajax({
                    url: "user_ajax.php",
                    method: "POST",
                    data: {qid: qid, exam_id: exam_id},
                    success: function (json) {
                        question_id = qid;
                        var jj = JSON.parse(json) //DECODING JSON TO READ DATA WITH (KEY)
                        qcounter++;
                        if(qcounter >totalQuestion){
                            console.log("Question End");
                            setTimeout(CompleteExam,1000);
                            //$('#questionform').hide();
                            return;
                        }
                        $('#qtitle').text("Question "+qcounter+": "+jj.question_title);
                        $('#answ1').text(jj.answer_title_a);
                        $('#answ2').text(jj.answer_title_b);
                        $('#answ3').text(jj.answer_title_c);
                        $('#answ4').text(jj.answer_title_d);
                        $('.btnsubmit').attr('id',jj.nextqid); //STORE NEXT QUESTION_ID

                        console.log(totalQuestion);
                        $('#CountDown').attr("data-timer", jj.question_duration).TimeCircles().start();
                        answer = jj.right_answer;
                        console.log(jj.right_answer);
                        $("#CountDown").TimeCircles().restart();
                        $('#duration').TimeCircles().start();
                        $("#answerStatus").fadeOut();
                    }
                })
            }
            //EVENT ON SUBMIT BUTTON CLICKED
            $(".btnsubmit").click(function () {
                $("#CountDown").TimeCircles().stop();
                //console.log(answer);
                var selectedAnswer = $("input[name='answer']:checked").val();

                if (selectedAnswer == answer) {
                   alert(question_id);
                    $.ajax
                    ({
                    url: "session.php",
                    type: "POST",
                    data: {qid: question_id},
                    success: function (data) {
                        alert(data);
                    },
                    error: function (request, status, error) {
                   alert("There was an error: ", request.responseText);
                     }
                    });
                    
                    //IF USER SELECT RIGHT ANSWER
                    //write cookies here 
                    
                    
                    
                    questionid = $(this).attr('id');
                    
                    //ecriture le cookie en cas de bonne reponse
                    /*
                    if (isset($_COOKIE['syntaxmap']))
                    {
                    $arr = unserialize($_COOKIE['syntaxmap']);
                    }
                    $arr[] = array($_GET['course_id'],',',questionid);
                    setcookie('syntaxmap', serialize($arr),'/');*/
                    
                    
                    $(".custom-radio").prop("checked", false);
                    $(".btnsubmit").attr("disabled", true);
                    $("#answerStatus").text("Your Answer Is: TRUE").fadeIn();
                    $('#duration').TimeCircles().stop();
                    setTimeout(LoadNextQuestion,2000);

                } else {
                    
                    //IF USER SELECT WRONG ANSWER
                    $("#answerStatus").text("Your Answer Is: FALSE").fadeIn();
                    $("#quiztest").prop("disabled", true);
                    $('#duration').TimeCircles().stop();
                    setTimeout(redirect, 2000);
                }
                //LOAD NEXT QUESTION WITH QUESTION ID
                function LoadNextQuestion() {
                    load_data(questionid)
                }
            })
        });
        //REDIRECT FUNC AND BACK TO MAIN COURSE
        function redirect() {
            $("#CountDown").TimeCircles().end().fadeOut();
            window.location.replace("index.php")

        }
        function CompleteExam(){
            $('#questionform').fadeOut();
            $('#completeexam').fadeIn();
        }
        //ENABLE SUBMIT BUTTON ON EACH ANSWER SELECTED
        $("input[type='radio']").click(function () {
            $(".btnsubmit").attr("disabled", false);
        })

        //REDIRECT ON TIMES UP
        $("#CountDown").TimeCircles({count_past_zero: false}).addListener(countdownComplete);
        function countdownComplete(unit, value, total) {
            if (total <= 0) {
                setTimeout(redirect,2000)
            }
        };
        $("#duration").TimeCircles({count_past_zero: false}).addListener(countdownComplete);
        function countdownComplete(unit, value, total) {
            if (total <= 0) {
                setTimeout(redirect,2000)
            }
        };
    </script>
