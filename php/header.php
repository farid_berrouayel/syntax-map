<html lang="en">
<head>
    <meta>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.css">
    <script src="../js/jquery-3.5.0.min.js"></script>
    <script src="../bootstrap/js/bootstrap.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../style/style.css">
    <script src="../js/TimeCircles.js"></script>
    <link rel="stylesheet" href="../style/TimeCircles.css" type="text/css"/>

    <title>Quiz Project</title>

<nav  class="navbar navbar-dark bg-dark" >
    <h1 style="color: white">Tenses Map</h1>
</nav>
<br>
</head>