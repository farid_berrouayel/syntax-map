<?php
//LOADQUESTION.PHP

include "../function/connections.php";
$qid = '';
$output = [];
$mainexam = [];
$time = '';
$answer ='';

//on lit le cookie

if(isset($_SESSION["syntaxmap"])){ 
$comma_separated=$_SESSION["syntaxmap"];

$comma_separated= substr($comma_separated,0,-1);



}


if(isset($_POST["examid"])){
    $duration_query = "SELECT * FROM `online_exam_table` WHERE `online_exam_id`=$_POST[examid] ";
    $duration_connection = mysqli_query($conn,$duration_query);
    $duration_result = mysqli_fetch_assoc($duration_connection);
    $totalcount_query = "SELECT * FROM `question_table` WHERE `online_exam_id`=$_POST[examid]";
    $totalcount_con = mysqli_query($conn,$totalcount_query);
    $total = mysqli_num_rows($totalcount_con);
    $duration = $duration_result["online_exam_duration"];
    $mainexam["totalduration"] = $duration;
    $mainexam["totalQuestionItems"] = $total;
    //print_r($mainexam);
    echo json_encode($mainexam);
    return;
}
if(isset($_POST["qid"])) {
    if ($_POST["qid"] == '') {


        // WHEN WE WANT TO LOAD FIRST QUESTION, WE DONT HAVE QUESTION_ID. SO SHOULD BE USE EXAM_ID TO SHOW FIRST QUESTION
        /*if(isset($_SESSION["syntaxmap"]))
        {*/
        //$questionsquery = "SELECT * FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and question_id not in (".$comma_separated.") order by rand()  limit 1";
        /*}
        else
        {*/
         $questionsquery = "SELECT * FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] order by rand()  limit 1";   
        //}   
        $questionsresult = mysqli_query($conn, $questionsquery);
        $questionsdetail = mysqli_fetch_assoc($questionsresult);
        //AFTER FIND FIRST QUESTION, CAN FIND SECOND QUESTION_ID WITH BELOW QUERY!
        /*if(isset($_SESSION["syntaxmap"]))
        {*/
        //$nexqid = "SELECT question_id FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and `question_id`>$questionsdetail[question_id] and question_id not in(".$comma_separated.") order by question_id asc  limit 1";
        /*}
        else
        {*/
         $nexqid = "SELECT question_id FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and `question_id`>$questionsdetail[question_id]   order by question_id asc  limit 1";
        //}
        $nexqidresult = mysqli_query($conn, $nexqid);
        $nexqdetail = mysqli_fetch_assoc($nexqidresult);
    } else {
        //WHEN WE HAVE QUESTION_ID, USE THIS QUERY TO GET QUESTION DETAILS.
        /*if(isset($_SESSION["syntaxmap"]))
        {*/
        //$questionsquery = "SELECT * FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and question_id not in (".$comma_separated.") order by rand()  limit 1";
        /*}
        else
        {*/
         $questionsquery = "SELECT * FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] order by rand()  limit 1";   
       // }    
        
        $questionsresult = mysqli_query($conn, $questionsquery);
        $questionsdetail = mysqli_fetch_assoc($questionsresult);
        /*if(isset($_SESSION["syntaxmap"]))
        {*/
        //$nexqid = "SELECT question_id FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and `question_id`>$questionsdetail[question_id] and question_id not in(".$comma_separated.") order by question_id asc  limit 1";
        /*}
        else
        {*/
         $nexqid = "SELECT question_id FROM `question_table` WHERE `online_exam_id`=$_POST[exam_id] and `question_id`>$questionsdetail[question_id]   order by question_id asc  limit 1";
        //}
        $nexqidresult = mysqli_query($conn, $nexqid);
        $nexqdetail = mysqli_fetch_assoc($nexqidresult);
    }
//LOAD ALL QUESTION DETAILS IN ONE ARRAY VARIABLE WITH LOOP TO SET {KEY} AND {VALUE}
    foreach ($questionsdetail as $key => $value) {
        $output[$key] = $value;
    }
//ADD NEXT QUESTION_ID TO ARRAY VARIABLE
    $output["nextqid"] = $nexqdetail["question_id"];
//CONVERT ARRAY TO JSON
    echo json_encode($output);
}
?>
